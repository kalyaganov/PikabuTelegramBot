using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reactive.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PikabuTelegramBot
{
    public class PikabuBot
    {
        private readonly TelegramBotClient _bot;
        private readonly string _botName;
        private readonly string _botanToken;
        private readonly ApiLikeThing _api;
        private readonly HttpClient _httpClient = new HttpClient();
        private Regex _commandWithArgumentPattern = new Regex(@"^\/[\w]* (.*)$");

        public PikabuBot(Configuration configuration)
        {
            _botanToken = configuration.BotToken;
            _botName = configuration.BotName;
            _bot = new TelegramBotClient(configuration.BotToken);
            _bot.OnMessage += HandleMessage;

            _api = new ApiLikeThing();
        }

        public void Run()
        {
            Console.WriteLine("Bot started");
            _bot.StartReceiving();
            Console.ReadLine();
            Console.WriteLine("Bot exit");
            _bot.StopReceiving();
        }

        private async void HandleMessage(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            if (message == null || message.Type != MessageType.TextMessage) return;
            if (message.Text.StartsWith("/start") || message.Text.StartsWith("/start@" + _botName))
            {
                await SendBotanEvent(message.From.Id, "Command: start");
                await _bot.SendTextMessageAsync(message.Chat.Id,
@"<b>Список команд бота:</b>
/start - команды бота
/top - три лучших поста на данный момент
/random - случайный пост
/search <i>текст</i> - случайный найденный пост
/tag <i>тег</i> - случайный пост с тегом <i>тег</i>

У нас есть свой пикабушный чат - @pikabu_chat", parseMode: ParseMode.Html);
            }
            else if (message.Text.StartsWith("/top") || message.Text.StartsWith("/top@" + _botName))
            {
                await SendBotanEvent(message.From.Id, "Command: top");
                await _bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                _api.Top().Subscribe(
                    async (posts) =>
                    {
                        foreach (var post in posts)
                        {
                            await SendPostPretty(message, post);
                        }
                    },
                    async (error) => await _bot.SendTextMessageAsync(message.Chat.Id, $"Error {error.Message}"));
            }
            else if (message.Text.StartsWith("/random") || message.Text.StartsWith("/random@" + _botName))
            {
                await SendBotanEvent(message.From.Id, "Command: random");
                await _bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                _api.Random().Subscribe(
                    async (post) =>
                    {
                        await SendPostPretty(message, post);
                    },
                    async (error) => await _bot.SendTextMessageAsync(message.Chat.Id, $"Error {error.Message}", disableWebPagePreview: true));
            }
            else if (message.Text.StartsWith("/search") || message.Text.StartsWith("/search@" + _botName))
            {
                await SendBotanEvent(message.From.Id, "Command: search");
                var matches = _commandWithArgumentPattern.Match(message.Text);
                if (matches.Groups.Count != 2)
                {
                    await _bot.SendTextMessageAsync(message.Chat.Id, $"Не правильный запрос. Надо вот так: /search <i>текст</i>", parseMode: ParseMode.Html);
                }
                else
                {
                    await _bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    var query = matches.Groups[matches.Groups.Count - 1].Value;
                    _api.Search(query).Subscribe(
                        async (posts) =>
                        {
                            if (posts.Count == 0)
                            {
                                await _bot.SendTextMessageAsync(message.Chat.Id, "По запросу <i>{query}</i> я ничего не нашёл ;(", parseMode: ParseMode.Html);
                            }
                            else
                            {
                                foreach (var post in posts)
                                {
                                    await SendPostPretty(message, post);
                                }
                            }
                        },
                        async (error) => await _bot.SendTextMessageAsync(message.Chat.Id, $"Error {error.Message}")
                    );
                }
            }
            else if (message.Text.StartsWith("/tag") || message.Text.StartsWith("/tag@" + _botName))
            {
                await SendBotanEvent(message.From.Id, "Command: tag");
                var matches = _commandWithArgumentPattern.Match(message.Text);
                if (matches.Groups.Count != 2)
                {
                    await _bot.SendTextMessageAsync(message.Chat.Id, $"Не правильный запрос. Надо вот так: /tag <i>тег</i>", parseMode: ParseMode.Html);
                }
                else
                {
                    await _bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    var tagName = matches.Groups[matches.Groups.Count - 1].Value;
                    _api.Tags(tagName).Subscribe(
                        async (posts) =>
                        {
                            if (posts.Count == 0)
                            {
                                await _bot.SendTextMessageAsync(message.Chat.Id, "По тегу <i>{tagName}</i> я ничего не нашёл ;(", parseMode: ParseMode.Html);
                            }
                            else
                            {
                                foreach (var post in posts)
                                {
                                    await SendPostPretty(message, post);
                                }
                            }
                        },
                        async (error) => await _bot.SendTextMessageAsync(message.Chat.Id, $"Error {error.Message}")
                    );
                }
            }
        }

        private async Task SendPostPretty(Message message, PostItem post)
        {
            await _bot.SendTextMessageAsync(message.Chat.Id, post.Url);
        }

        private async Task<bool> SendBotanEvent(int userId, string eventName)
        {
            try
            {
                if (string.IsNullOrEmpty(_botanToken)) return false;
                var postBody = new
                {
                    token = _botanToken,
                    uid = userId,
                    name = eventName
                };
                var content = new StringContent(JsonConvert.SerializeObject(postBody), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync($"https://api.botan.io/track", content);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                //Ignore
            }
            return false;
        }

        ///
        ///Configuration file
        public sealed class Configuration
        {
            public string BotToken { get; set; }
            public string BotName { get; set; }
            public string BotanToken { get; set; }
        }
    }
}
